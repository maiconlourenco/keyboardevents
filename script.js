let boxTop = 10;
let boxLeft = 10;


document.addEventListener('keydown', (event) => {

    let maxWidth = window.innerWidth;
    let maxHeight = window.innerHeight;
    
    let boxWidth = document.getElementById('box').offsetWidth
    let boxHeight = document.getElementById('box').offsetHeight

  const keyName = event.key;
    if (keyName == 'ArrowDown' && (boxTop +10) <= (maxHeight - boxHeight)) {
        boxTop += 10
    } 
    
    if (keyName == 'ArrowUp' && boxTop > 10) {
        boxTop -= 10
    }

    if (keyName == 'ArrowRight' && (boxLeft + 10) <= (maxWidth - boxWidth)) {
        boxLeft += 10
    } 
    
    if (keyName == 'ArrowLeft' && boxLeft > 10) {
        boxLeft -= 10
    }

  document.getElementById("box").style.top = boxTop + "px"
  document.getElementById("box").style.left = boxLeft + "px"

  console.log('keydown event\n\n' + 'key: ' + keyName);
});
